//scheduled event
addEventListener('scheduled', event => {
  event.waitUntil(
    handleSchedule(event.scheduledTime)
  )
})

async function handleSchedule(scheduledDate) {
  //application key and ID are set as encrypted environment variables
  let headers = new Headers();
  headers.append('Authorization', 'Basic' + btoa(applicationKeyID + ":" + applicationKey));

  var requestOptions = {
    method: 'GET',
    headers: headers,
  };

  //apiURL is set as an encrypted environment variable
  const response = await fetch(apiURL, requestOptions).then(response => {
    return response;
  })

  const responseParsed = await response.text().then(res => {
    return JSON.parse(res);
  })

  await STORAGE_ACCESS.put("authorizationToken", responseParsed.authorizationToken);
  await STORAGE_ACCESS.put("bucketID", responseParsed.allowed.bucketId);
  await STORAGE_ACCESS.put("downloadURL", responseParsed.downloadUrl);

  console.log(responseParsed)
}
