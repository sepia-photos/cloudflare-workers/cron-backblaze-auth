# cron-backblaze-auth

Sepia's backend consists of serverless functions, MongoDB, and object storage. The object storage is hosted on Backblaze B2, and all requests to the API are handled by CloudFlare workers. When necessary, information is passed on to IBM cloud functions which can interface with the database.

This worker uses credentials with Backblaze to grab the authorization token, which it then stores it in [Workers KV](https://developers.cloudflare.com/workers/runtime-apis/kv) so that all other workers can authorize themselves with the B2 API. It's scheduled to run every 20 hours.

## Deployment

Right now there is no deployment pipeline configuration setup. To deploy this yourself you either create a Cloudflare Worker and manually paste in the contents of index.js or configure [wrangler](https://developers.cloudflare.com/workers/cli-wrangler/install-update), Cloudflare's cli tool for workers.
